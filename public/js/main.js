let users = [
	{
		name: 'Tom Smith',
		company: 'Abc Corp',
		role: 'CEO',
		tenure: 15,
	},
	{
		name: 'John Doe',
		company: 'Xyz Corp',
		role: 'CEO',
		tenure: 10,
	},
	{
		name: 'Marcus Gutierrez',
		company: 'Abc Corp',
		role: 'Manager',
		tenure: 13,
	},
	{
		name: 'Janice Cooper',
		company: '123 Corp',
		role: 'CEO',
		tenure: 17,
	},
	{
		name: 'Tina Aldridge',
		company: 'Xyz Corp',
		role: 'Manager',
		tenure: 5,
	},
	{
		name: 'Cindy Nguyen',
		company: 'Xyz Corp',
		role: 'Programmer',
		tenure: 3,
	},
	{
		name: 'Jackson Green',
		company: 'Abc Corp',
		role: 'Programmer',
		tenure: 2,
	},
	{
		name: 'Adam Palmer',
		company: '123 Corp',
		role: 'Programmer',
		tenure: 1,
	},
];
let totalUsers;
const usersPerPage = 3;
let curPage = 0;
let totalPages;
let allCompanies;

(() => {
	users = sortUsers(users);
	totalUsers = 0; // update to set this as the total number of users
	totalPages = determineTotalNumberOfPages(totalUsers, usersPerPage);
	allCompanies = generateArrayOfUniqueCompanies(users);

	populateCurrentUsersIntoHtml();
	populateAllCompaniesIntoCompaniesDiv();
	setupButtons();
})();

function sortUsers(users) {
	// return an array of users sorted by company first, then name
	return [];
}

function determineTotalNumberOfPages(totalUsers, perPage) {
	// return the total number of possible pages
	return 1000;
}

function generateArrayOfUniqueCompanies(users) {
	// return an array of unique corporations from all users, sorted
	return [];
}

function populateCurrentUsersIntoHtml() {
	// populate the #usersContent div with the currently visible users based on the pagination and give each user div a class name of user
	// each user should display Company, Name, Role, Tenure, in that order.
	// the currently defined .user class has a css grid setup already
	// add a class to user user sub div where the class equals the key name in the user object

	// once the users have been added, call to update the pagination text
	updatePaginationText();
}

function updatePaginationText() {
	// this should update the text inside of #pagText to show current page and total number of pages
}

function populateAllCompaniesIntoCompaniesDiv() {
	// populate the #companies div with the unique companies in the allCompanies array
}

function setupButtons() {
	// setup each button to navigate forward or back within the users pagination when clicked
}
